from simple_clingo import SimpleClingo

# Hitori is a puzzle where the goal is to cross out some numbers, such that:
# The remaining part is all connected together
# There do not exist two of the same uncrossed numbers in a row or column
# Two neighboring cells are not crossed out


def run():
    hitori = [
        [14, 2, 1, 2, 4],
        [18, 5, 4, 14, 5],
        [14, 5, 1, 1, 5],
        [4, 1, 2, 4, 14],
    ]
    solutions = solve_hitori(hitori, verbose=True)

    for solution in solutions:
        print()
        print_hitori(hitori, solution)


def solve_hitori(hitori, verbose=False, n_solutions=100):
    clingo = SimpleClingo()
    clingo.load_path('examples/6_hitori.lp')

    # We need to add all the known information as an initial/3 predicate
    for r, row in enumerate(hitori):
        clingo.add_clause('row({}).'.format(r))
        for c, value in enumerate(row):
            if r == 0:
                clingo.add_clause('column({}).'.format(c))
            clingo.add_clause('value({},{},{}).'.format(r, c, value))

    models = clingo.solve(n_solutions, randomness=0.0, verbose=verbose)
    if verbose:
        if len(models) == n_solutions:
            print("At least {} solutions found.".format(n_solutions))
        else:
            print("{} solution{} found".format(len(models), 's' if len(models) > 1 else ''))

    return models


def print_hitori(hitori, model):
    hitori = [row[:] for row in hitori]
    just = max([max([len(str(el)) for el in row]) for row in hitori])

    for row, column in model['crossed_out']:
        hitori[row][column] = '#'.rjust(just, '#')

    for row in hitori:
        print(' | '.join([str(x).rjust(just) for x in row]))

if __name__ == '__main__':
    run()